---------------------------------------------------------------------------
--********************************* UDT *********************************--
---------------------------------------------------------------------------

--------------------------------------------------------------
--//////////////////// CREATE OBJ TYPES ////////////////////--
--------------------------------------------------------------
------------------------------------------------------
--  TYPE TYPE_TEMPLATE
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_TEMPLATE AS OBJECT
(
	ID_TEMPLATE NUMBER(8) ,
	NAME VARCHAR2(100) ,
	URL VARCHAR2(300)
);
/
------------------------------------------------------
--  TYPE TYPE_MAIN_LINK
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_MAIN_LINK AS OBJECT
(
	ID_MAIN_LINK NUMBER(8) ,
	NAME VARCHAR2(100) ,
	URL VARCHAR2(300),
    LINK_ORDER NUMBER,
    TEMPLATE_REF REF TYPE_TEMPLATE
);
/
------------------------------------------------------
--  TYPE TYPE_SECONDARY_LINK
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_SECONDARY_LINK AS OBJECT
(
	ID_SECONDARY_LINK   NUMBER(8) ,
	NAME VARCHAR2(100) ,
	URL VARCHAR2(300),
    LINK_ORDER NUMBER,
    MAIN_LINK_REF REF TYPE_MAIN_LINK
);
/
------------------------------------------------------
--  TYPE TYPE_CONTENT
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_CONTENT AS OBJECT
(
	ID_CONTENT NUMBER(8) ,
	MAIN_TITLE VARCHAR2(100) ,
    SECONDARY_LINK_REF REF TYPE_SECONDARY_LINK
);
/
------------------------------------------------------
--  TYPE TYPE_ARTICLE
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_ARTICLE AS OBJECT
(
	ID_ARTICLE NUMBER(8) ,
	NAME VARCHAR2(100) ,
    CONTENT_REF REF TYPE_CONTENT
);
/
------------------------------------------------------
--  TYPE TYPE_PARAGRAPH
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_PARAGRAPH AS OBJECT
(
	ID_PARAGRAPH NUMBER(8) ,
	NAME VARCHAR2(100) ,
	CONTENT VARCHAR2(1500),
    ARTICLE_REF REF TYPE_ARTICLE
);
/
------------------------------------------------------
--  TYPE TYPE_IMAGE
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_IMAGE AS OBJECT
(
	ID_IMAGE NUMBER(8) ,
	NAME VARCHAR2(100) ,
	SUFFIX VARCHAR2(5),
    ARTICLE_REF REF TYPE_ARTICLE
);
/
------------------------------------------------------
--  TYPE TYPE_DATA
------------------------------------------------------
CREATE OR REPLACE TYPE TYPE_DATA AS OBJECT
(
	ID_DATA NUMBER(8) ,
	DATA BLOB ,
    IMAGE_REF REF TYPE_IMAGE
);
/

---------------------------------------------------------------
--//////////////////// CREATE OBJ TABLES ////////////////////--
---------------------------------------------------------------
CREATE TABLE OBJ_TEMPLATE OF TYPE_TEMPLATE;
CREATE TABLE OBJ_MAIN_LINK OF TYPE_MAIN_LINK;
CREATE TABLE OBJ_SECONDARY_LINK OF TYPE_SECONDARY_LINK;
CREATE TABLE OBJ_CONTENT OF TYPE_CONTENT;
CREATE TABLE OBJ_ARTICLE OF TYPE_ARTICLE;
CREATE TABLE OBJ_PARAGRAPH OF TYPE_PARAGRAPH;
CREATE TABLE OBJ_IMAGE OF TYPE_IMAGE;
CREATE TABLE OBJ_DATA OF TYPE_DATA;




--------------------------------------------------------
--  DDL for Index SYS_C00266661
--------------------------------------------------------
  CREATE UNIQUE INDEX "SYS_C00266661" ON "OBJ_ARTICLE" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266660" ON "OBJ_CONTENT" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266664" ON "OBJ_DATA" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266663" ON "OBJ_IMAGE" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266658" ON "OBJ_MAIN_LINK" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266662" ON "OBJ_PARAGRAPH" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266659" ON "OBJ_SECONDARY_LINK" ("SYS_NC_OID$");
  CREATE UNIQUE INDEX "SYS_C00266657" ON "OBJ_TEMPLATE" ("SYS_NC_OID$");

--------------------------------------------------------
--  Constraints for Table OBJ_ARTICLE
--------------------------------------------------------
  ALTER TABLE "OBJ_ARTICLE" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_CONTENT" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_DATA" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_IMAGE" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_MAIN_LINK" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_PARAGRAPH" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_SECONDARY_LINK" ADD UNIQUE ("SYS_NC_OID$") ENABLE;
  ALTER TABLE "OBJ_TEMPLATE" ADD UNIQUE ("SYS_NC_OID$") ENABLE;



-------------------------------------------------------------
--//////////////////// FILL OBJ TABLES ////////////////////--
-------------------------------------------------------------
------------------------------------------------------
--  INSERT INTO OBJ_TEMPLATE
------------------------------------------------------
INSERT INTO OBJ_TEMPLATE SELECT * FROM T_TEMPLATE;

------------------------------------------------------
--  INSERT INTO OBJ_MAIN_LINK
------------------------------------------------------
INSERT INTO OBJ_MAIN_LINK 
SELECT M.ID_MAIN_LINK,M.NAME,M.LINK_ORDER,M.URL, (
    SELECT REF (O) 
        FROM OBJ_TEMPLATE O 
        WHERE O.ID_TEMPLATE = M.ID_TEMPLATE
)
FROM T_MAIN_LINK M ;

------------------------------------------------------
--  INSERT INTO OBJ_SECONDARY_LINK
------------------------------------------------------
INSERT INTO OBJ_SECONDARY_LINK 
SELECT SC.ID_SECONDARY_LINK, SC.NAME, SC.URL, SC.LINK_ORDER, (
    SELECT REF(O) 
    FROM OBJ_MAIN_LINK O 
    WHERE O.ID_MAIN_LINK = SC.ID_MAIN_LINK
) 
FROM T_SECONDARY_LINK SC;

------------------------------------------------------
--  INSERT INTO OBJ_CONTENT
------------------------------------------------------
INSERT INTO OBJ_CONTENT 
SELECT CN.ID_CONTENT, CN.MAIN_TITLE, (
    SELECT REF(O) 
    FROM OBJ_SECONDARY_LINK O 
    WHERE O.ID_SECONDARY_LINK = CN.ID_SECONDARY_LINK
) 
FROM T_CONTENT CN;

------------------------------------------------------
--  INSERT INTO OBJ_ARTICLE
------------------------------------------------------
INSERT INTO OBJ_ARTICLE 
SELECT ART.ID_ARTICLE, ART.NAME, (
    SELECT REF(O) 
    FROM OBJ_CONTENT O 
    WHERE O.ID_CONTENT = ART.ID_CONTENT
) 
FROM T_ARTICLE ART;

------------------------------------------------------
--  INSERT INTO OBJ_PARAGRAPH
------------------------------------------------------
INSERT INTO OBJ_PARAGRAPH 
SELECT PA.ID_PARAGRAPH, PA.NAME, PA.CONTENT, (
    SELECT REF(O) 
    FROM OBJ_ARTICLE O 
    WHERE O.ID_ARTICLE = PA.ID_ARTICLE
) 
FROM T_PARAGRAPH PA;

------------------------------------------------------
--  INSERT INTO OBJ_IMAGE
------------------------------------------------------
INSERT INTO OBJ_IMAGE 
SELECT IM.ID_IMAGE, IM.NAME, IM.SUFFIX, (
    SELECT REF(O) 
    FROM OBJ_ARTICLE O 
    WHERE O.ID_ARTICLE = IM.ID_ARTICLE
) 
FROM T_IMAGE IM;

------------------------------------------------------
--  INSERT INTO OBJ_DATA
------------------------------------------------------
INSERT INTO OBJ_DATA 
SELECT DT.ID_DATA, DT.DATA, (
    SELECT REF(O) 
    FROM OBJ_IMAGE O 
    WHERE O.ID_IMAGE = DT.ID_IMAGE
) 
FROM T_DATA DT;

