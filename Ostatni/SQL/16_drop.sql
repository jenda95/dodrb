-------------------------------------------------------------------------------
--//////////////////////////// CLEAN-UP DATABASE ////////////////////////////--
-------------------------------------------------------------------------------

---------------------------------------
-- DROP TRIGGERS
---------------------------------------
DROP TRIGGER "T_DATA_TRG";
DROP TRIGGER "T_IMAGE_TRG";
DROP TRIGGER "T_PARAGRAPH_TRG";
DROP TRIGGER "T_ARTICLE_TRG";
DROP TRIGGER "T_CONTENT_TRG";
DROP TRIGGER "T_SECONDARY_LINK_TRG";
DROP TRIGGER "T_MAIN_LINK_TRG";
DROP TRIGGER "T_TEMPLATE_TRG";

---------------------------------------
-- DROP SEQUENCES
---------------------------------------
DROP SEQUENCE "SEQ_DATA";
DROP SEQUENCE "SEQ_IMAGE";
DROP SEQUENCE "SEQ_PARAGRAPH";
DROP SEQUENCE "SEQ_ARTICLE";
DROP SEQUENCE "SEQ_CONTENT";
DROP SEQUENCE "SEQ_SECONDARY_LINK";
DROP SEQUENCE "SEQ_MAIN_LINK";
DROP SEQUENCE "SEQ_TEMPLATE";
DROP SEQUENCE "SEQ_ORDER_ML";
DROP SEQUENCE "SEQ_ORDER_SL";

---------------------------------------
-- DROP TABLES
---------------------------------------
DROP TABLE "T_DATA";
DROP TABLE "T_IMAGE";
DROP TABLE "T_PARAGRAPH";
DROP TABLE "T_ARTICLE";
DROP TABLE "T_CONTENT";
DROP TABLE "T_SECONDARY_LINK";
DROP TABLE "T_MAIN_LINK";
DROP TABLE "T_TEMPLATE";

---------------------------------------
-- DROP OBJ TABLES
---------------------------------------
DROP TABLE "OBJ_DATA";
DROP TABLE "OBJ_IMAGE";
DROP TABLE "OBJ_PARAGRAPH";
DROP TABLE "OBJ_ARTICLE";
DROP TABLE "OBJ_CONTENT";
DROP TABLE "OBJ_SECONDARY_LINK";
DROP TABLE "OBJ_MAIN_LINK";
DROP TABLE "OBJ_TEMPLATE";

---------------------------------------
-- DROP TYPES
---------------------------------------
DROP TYPE "TYPE_DATA";
DROP TYPE "TYPE_IMAGE";
DROP TYPE "TYPE_PARAGRAPH";
DROP TYPE "TYPE_ARTICLE";
DROP TYPE "TYPE_CONTENT";
DROP TYPE "TYPE_SECONDARY_LINK";
DROP TYPE "TYPE_MAIN_LINK";
DROP TYPE "TYPE_TEMPLATE";

---------------------------------------
-- DOPLNIT!!!
---------------------------------------
-- DROP PROCEDURE "POCET_LETENEK";
 DROP PACKAGE webovy_clanek;
