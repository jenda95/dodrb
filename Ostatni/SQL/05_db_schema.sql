----------------------------------------------------------------------------
--//////////////////////////// BUILD DATABASE ////////////////////////////--
----------------------------------------------------------------------------


---------------------------------------------------------------
--////////////////////// CREATE TABLES //////////////////////--
---------------------------------------------------------------
------------------------------------------------------
--  Table T_TEMPLATE
------------------------------------------------------
CREATE TABLE "T_TEMPLATE"
(	
    "ID_TEMPLATE" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"URL" VARCHAR2(300)
);

------------------------------------------------------
--  Table T_MAIN_LINK
------------------------------------------------------
CREATE TABLE "T_MAIN_LINK"
(	
    "ID_MAIN_LINK" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"LINK_ORDER" NUMBER, 
	"URL" VARCHAR2(300), 
	"ID_TEMPLATE" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_SECONDARY_LINK
------------------------------------------------------
CREATE TABLE "T_SECONDARY_LINK"
(	
    "ID_SECONDARY_LINK" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"URL" VARCHAR2(300), 
	"LINK_ORDER" NUMBER, 
	"ID_MAIN_LINK" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_CONTENT
------------------------------------------------------
CREATE TABLE "T_CONTENT"
(	
    "ID_CONTENT" NUMBER(8,0), 
	"MAIN_TITLE" VARCHAR2(100), 
	"ID_SECONDARY_LINK" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_ARTICLE
------------------------------------------------------
CREATE TABLE "T_ARTICLE"
(	
    "ID_ARTICLE" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"ID_CONTENT" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_PARAGRAPH
------------------------------------------------------
CREATE TABLE "T_PARAGRAPH"
(	
    "ID_PARAGRAPH" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"CONTENT" VARCHAR2(1500), 
	"ID_ARTICLE" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_IMAGE
------------------------------------------------------
CREATE TABLE "T_IMAGE"
(
    "ID_IMAGE" NUMBER(8,0), 
	"NAME" VARCHAR2(100), 
	"SUFFIX" VARCHAR2(5), 
	"ID_ARTICLE" NUMBER(8,0)
);

------------------------------------------------------
--  Table T_DATA
------------------------------------------------------
CREATE TABLE "T_DATA"
(	
    "ID_DATA" NUMBER(8,0), 
	"DATA" BLOB, 
	"ID_IMAGE" NUMBER(8,0)
);



------------------------------------------------------
--  CREATE INDEXES 
------------------------------------------------------
  CREATE UNIQUE INDEX "TEMPLATES_PK" ON "T_TEMPLATE" ("ID_TEMPLATE");
  CREATE UNIQUE INDEX "MAIN_LINK_PK" ON "T_MAIN_LINK" ("ID_MAIN_LINK");
  CREATE UNIQUE INDEX "SECONDARY_LINK_PK" ON "T_SECONDARY_LINK" ("ID_SECONDARY_LINK");
  CREATE UNIQUE INDEX "CONTENT_PK" ON "T_CONTENT" ("ID_CONTENT");
  CREATE UNIQUE INDEX "ARTICLE_PK" ON "T_ARTICLE" ("ID_ARTICLE");
  CREATE UNIQUE INDEX "PARAGRAPH_PK" ON "T_PARAGRAPH" ("ID_PARAGRAPH");
  CREATE UNIQUE INDEX "IMAGE_PK" ON "T_IMAGE" ("ID_IMAGE");
  CREATE UNIQUE INDEX "DATA_PK" ON "T_DATA" ("ID_DATA");

  CREATE UNIQUE INDEX "TEMPLATE_UK" ON "T_TEMPLATE" ("URL");
  CREATE UNIQUE INDEX "MAIN_LINK_UK" ON "T_MAIN_LINK" ("URL");
  CREATE UNIQUE INDEX "SECONDARY_LINK_UK" ON "T_SECONDARY_LINK" ("URL");

------------------------------------------------------
--  CREATE PRIMARY KEYS
------------------------------------------------------
  ALTER TABLE "T_TEMPLATE" ADD CONSTRAINT "TEMPLATE_PK" PRIMARY KEY ("ID_TEMPLATE") ENABLE;
  ALTER TABLE "T_MAIN_LINK" ADD CONSTRAINT "MAIN_LINK_PK" PRIMARY KEY ("ID_MAIN_LINK") ENABLE;
  ALTER TABLE "T_SECONDARY_LINK" ADD CONSTRAINT "SECONDARY_LINK_PK" PRIMARY KEY ("ID_SECONDARY_LINK") ENABLE;
  ALTER TABLE "T_CONTENT" ADD CONSTRAINT "CONTENT_PK" PRIMARY KEY ("ID_CONTENT") ENABLE;
  ALTER TABLE "T_ARTICLE" ADD CONSTRAINT "ARTICLE_PK" PRIMARY KEY ("ID_ARTICLE") ENABLE;
  ALTER TABLE "T_PARAGRAPH" ADD CONSTRAINT "PARAGRAPH_PK" PRIMARY KEY ("ID_PARAGRAPH") ENABLE;
  ALTER TABLE "T_IMAGE" ADD CONSTRAINT "IMAGE_PK" PRIMARY KEY ("ID_IMAGE") ENABLE;
  ALTER TABLE "T_DATA" ADD CONSTRAINT "DATA_PK" PRIMARY KEY ("ID_DATA") ENABLE;


------------------------------------------------------
--  CREATE FOREIGN KEYS
------------------------------------------------------
  ALTER TABLE "T_MAIN_LINK" ADD CONSTRAINT "MAIN_LINK_FK" FOREIGN KEY ("ID_TEMPLATE") REFERENCES "T_TEMPLATE" ("ID_TEMPLATE") ENABLE;
  ALTER TABLE "T_SECONDARY_LINK" ADD CONSTRAINT "SECONDARY_LINK_FK" FOREIGN KEY ("ID_MAIN_LINK") REFERENCES "T_MAIN_LINK" ("ID_MAIN_LINK") ENABLE;
  ALTER TABLE "T_CONTENT" ADD CONSTRAINT "CONTENT_FK" FOREIGN KEY ("ID_SECONDARY_LINK") REFERENCES "T_SECONDARY_LINK" ("ID_SECONDARY_LINK") ENABLE; 
  ALTER TABLE "T_ARTICLE" ADD CONSTRAINT "ARTICLE_FK" FOREIGN KEY ("ID_CONTENT") REFERENCES "T_CONTENT" ("ID_CONTENT") ENABLE;
  ALTER TABLE "T_PARAGRAPH" ADD CONSTRAINT "PARAGRAPH_FK" FOREIGN KEY ("ID_ARTICLE") REFERENCES "T_ARTICLE" ("ID_ARTICLE") ENABLE;
  ALTER TABLE "T_IMAGE" ADD CONSTRAINT "IMAGE_FK" FOREIGN KEY ("ID_ARTICLE") REFERENCES "T_ARTICLE" ("ID_ARTICLE") ENABLE;
  ALTER TABLE "T_DATA" ADD CONSTRAINT "DATA_FK" FOREIGN KEY ("ID_IMAGE") REFERENCES "T_IMAGE" ("ID_IMAGE") ENABLE;


------------------------------------------------------
--  CREATE UNIQUE KEYS
------------------------------------------------------
  ALTER TABLE "T_TEMPLATE" ADD CONSTRAINT "TEMPLATE_UK" UNIQUE ("URL") ENABLE;
  ALTER TABLE "T_MAIN_LINK" ADD CONSTRAINT "MAIN_LINK_UK" UNIQUE ("URL") ENABLE;
  ALTER TABLE "T_SECONDARY_LINK" ADD CONSTRAINT "SECONDARY_LINK_UK" UNIQUE ("URL") ENABLE;


------------------------------------------------------
-- CREATE SEQUENCES
------------------------------------------------------
CREATE SEQUENCE SEQ_TEMPLATE
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_MAIN_LINK
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_SECONDARY_LINK
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_CONTENT
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_ARTICLE
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_PARAGRAPH
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_IMAGE
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;
  
CREATE SEQUENCE SEQ_DATA
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_ORDER_ML
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE SEQUENCE SEQ_ORDER_SL
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

