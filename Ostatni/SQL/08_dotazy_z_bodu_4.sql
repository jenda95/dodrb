SET AUTOTRACE ON;

-- 1. seznam všech primární odkazů včetně všech jejich parametrů pro vykreslení na stránce

SELECT 
    ml.NAME AS "Název", 
    CONCAT(t.URL, ml.URL) AS "Absolutní URL", 
    t.name as "Název templatu", 
    ml.link_order as "Pořadí v templatu",
    COUNT(DISTINCT sl.id_secondary_link) AS "Počet pododkazů", 
    COUNT(DISTINCT pa.id_paragraph) AS "Počet paragrafů",
    COUNT(DISTINCT art.id_article) AS "Počet článků",
    COUNT(DISTINCT img.id_image) AS "Počet obrázků",
    COUNT(DISTINCT dt.id_data) AS "Počet datových soubotů"
FROM T_MAIN_LINK ml
JOIN T_TEMPLATE t  ON ML.ID_TEMPLATE = T.ID_TEMPLATE
JOIN T_SECONDARY_LINK sl ON ML.ID_MAIN_LINK = SL.ID_MAIN_LINK 
JOIN t_content co ON sl.id_secondary_link = co.id_secondary_link
JOIN t_article art ON co.id_content = art.id_content
JOIN t_paragraph pa ON art.id_article = pa.id_article
JOIN t_image img ON art.id_article = img.id_article
JOIN t_data dt ON img.id_image = dt.id_image
--WHERE t.id_template = 3
GROUP BY ML.NAME, CONCAT(T.URL, ML.URL), t.name, ml.link_order
ORDER BY t.name;


-- 2. seznam všech sekundárních odkazů včetně všech jejich parametrů pro vykreslení na stránce, které spadají pod určitý primární odkaz

SELECT 
    sl.name AS "Název", 
    CONCAT(CONCAT(t.URL, ml.URL),sl.url) AS "Absolutní URL", 
    t.name as "Název templatu", 
    ml.name as "Název main linku", 
    ml.link_order as "Pořadí main linku v templatu",
    sl.link_order as "Pořadí v main linku",
    COUNT(DISTINCT pa.id_paragraph) AS "Počet paragrafů",
    COUNT(DISTINCT art.id_article) AS "Počet článků",
    COUNT(DISTINCT img.id_image) AS "Počet obrázků",
    COUNT(DISTINCT dt.id_data) AS "Počet datových soubotů" 
FROM t_secondary_link sl
JOIN t_main_link ml  ON ml.id_main_link = sl.id_main_link
JOIN T_TEMPLATE t  ON ML.ID_TEMPLATE = T.ID_TEMPLATE
JOIN t_content co ON sl.id_secondary_link = co.id_secondary_link
JOIN t_article art ON co.id_content = art.id_content
JOIN t_paragraph pa ON art.id_article = pa.id_article
JOIN t_image img ON art.id_article = img.id_article
JOIN t_data dt ON img.id_image = dt.id_image

WHERE ml.id_main_link = 2 or ml.id_main_link = 13
GROUP BY ML.NAME, CONCAT(CONCAT(T.URL, ML.URL),sl.url), t.name, ml.link_order, sl.name, sl.link_order
ORDER BY t.name, sl.link_order;


-- 3. seznam všech článků, které spadají pod určitý sekundární odkaz
SELECT art.id_article as "ID artiklu", art.name as "Název artiklu"
FROM t_article art
JOIN T_CONTENT co ON co.id_content = art.id_content
JOIN T_SECONDARY_LINK sl ON co.id_secondary_link = sl.id_secondary_link
WHERE sl.id_secondary_link = 3
ORDER BY  sl.link_order;


-- 4. seznam všech obrázků, které se vážou k danému článku
SELECT img.id_image as "ID obrázku", img.name as "Název obrázku", img.suffix as "Přípona", count(distinct  dt.id_data) as "Počet částí obrázku"
FROM t_image img
JOIN t_article art  ON art.id_article = img.id_article
JOIN t_data dt ON img.id_image = dt.id_image
WHERE art.id_article = 10
group by img.id_image, img.name, img.suffix
order by img.id_image;



-- 5. seznam odstavců, které spadají pod konkrétní článek
SELECT pa.id_paragraph as "ID odstavce", pa.name as "Název odstavce", pa.content as "Obsah odstavce"
FROM t_paragraph pa
JOIN t_article art  ON art.id_article = pa.id_article
WHERE art.id_article = 4
order by pa.name;


SET AUTOTRACE OFF;