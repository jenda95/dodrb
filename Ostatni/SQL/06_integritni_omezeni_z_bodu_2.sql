-----------------------------------------------------------------------------
--************************* INTEGRITY CONSTRAINTS *************************--
-----------------------------------------------------------------------------

----------------------------------------------------------------
--//////////////////// CREATE CONSTRAINTS ////////////////////--
----------------------------------------------------------------
--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_TEMPLATE
--------------------------------------------------------
  ALTER TABLE "T_TEMPLATE" MODIFY ("ID_TEMPLATE" NOT NULL ENABLE);
  ALTER TABLE "T_TEMPLATE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_TEMPLATE" MODIFY ("URL" NOT NULL ENABLE);
    
--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_MAIN_LINK
--------------------------------------------------------
  ALTER TABLE "T_MAIN_LINK" MODIFY ("ID_MAIN_LINK" NOT NULL ENABLE);
  ALTER TABLE "T_MAIN_LINK" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_MAIN_LINK" MODIFY ("URL" NOT NULL ENABLE);
  ALTER TABLE "T_MAIN_LINK" MODIFY ("ID_TEMPLATE" NOT NULL ENABLE);

--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_SECONDARY_LINK
--------------------------------------------------------
  ALTER TABLE "T_SECONDARY_LINK" MODIFY ("ID_SECONDARY_LINK" NOT NULL ENABLE);
  ALTER TABLE "T_SECONDARY_LINK" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_SECONDARY_LINK" MODIFY ("URL" NOT NULL ENABLE);
  ALTER TABLE "T_SECONDARY_LINK" MODIFY ("ID_MAIN_LINK" NOT NULL ENABLE);
  
  --------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_CONTENT
--------------------------------------------------------
  ALTER TABLE "T_CONTENT" MODIFY ("ID_CONTENT" NOT NULL ENABLE);
  ALTER TABLE "T_CONTENT" MODIFY ("ID_SECONDARY_LINK" NOT NULL ENABLE);

--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_ARTICLE
--------------------------------------------------------
  ALTER TABLE "T_ARTICLE" MODIFY ("ID_ARTICLE" NOT NULL ENABLE);
  ALTER TABLE "T_ARTICLE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_ARTICLE" MODIFY ("ID_CONTENT" NOT NULL ENABLE);

--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_PARAGRAPH
--------------------------------------------------------
  ALTER TABLE "T_PARAGRAPH" MODIFY ("ID_PARAGRAPH" NOT NULL ENABLE);
  ALTER TABLE "T_PARAGRAPH" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_PARAGRAPH" MODIFY ("CONTENT" NOT NULL ENABLE);
  ALTER TABLE "T_PARAGRAPH" MODIFY ("ID_ARTICLE" NOT NULL ENABLE);

--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_IMAGE
--------------------------------------------------------
  ALTER TABLE "T_IMAGE" MODIFY ("ID_IMAGE" NOT NULL ENABLE);
  ALTER TABLE "T_IMAGE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_IMAGE" MODIFY ("SUFFIX" NOT NULL ENABLE);
  ALTER TABLE "T_IMAGE" ADD CONSTRAINT "SUFFIX" CHECK (SUFFIX NOT LIKE '%[^a-z]%');
  ALTER TABLE "T_IMAGE" MODIFY ("ID_ARTICLE" NOT NULL ENABLE);
  
--------------------------------------------------------
--  CONSTRAINTS FOR TABLE T_DATA
--------------------------------------------------------
  ALTER TABLE "T_DATA" MODIFY ("ID_DATA" NOT NULL ENABLE);
  ALTER TABLE "T_DATA" MODIFY ("DATA" NOT NULL ENABLE);
  ALTER TABLE "T_DATA" MODIFY ("ID_IMAGE" NOT NULL ENABLE);


-----------------------------------------------------------------
--////////////////////// CREATE TRIGGERS //////////////////////--
-----------------------------------------------------------------

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_TEMPLATE
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_TEMPLATE_TRG" 
BEFORE INSERT ON T_TEMPLATE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_TEMPLATE IS NULL THEN
      SELECT SEQ_TEMPLATE.NEXTVAL INTO :NEW.ID_TEMPLATE FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_TEMPLATE_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_MAIN_LINK
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_MAIN_LINK_TRG" 
BEFORE INSERT ON T_MAIN_LINK 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_MAIN_LINK IS NULL THEN
      SELECT SEQ_MAIN_LINK.NEXTVAL INTO :NEW.ID_MAIN_LINK FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_MAIN_LINK_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_SECONDARY_LINK
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_SECONDARY_LINK_TRG" 
BEFORE INSERT ON T_SECONDARY_LINK 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_SECONDARY_LINK IS NULL THEN
      SELECT SEQ_SECONDARY_LINK.NEXTVAL INTO :NEW.ID_SECONDARY_LINK FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_SECONDARY_LINK_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_CONTENT
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_CONTENT_TRG" 
BEFORE INSERT ON T_CONTENT 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_CONTENT IS NULL THEN
      SELECT SEQ_CONTENT.NEXTVAL INTO :NEW.ID_CONTENT FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_CONTENT_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_ARTICLE
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_ARTICLE_TRG" 
BEFORE INSERT ON T_ARTICLE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_ARTICLE IS NULL THEN
      SELECT SEQ_ARTICLE.NEXTVAL INTO :NEW.ID_ARTICLE FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_ARTICLE_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_PARAGRAPH
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_PARAGRAPH_TRG" 
BEFORE INSERT ON T_PARAGRAPH 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_PARAGRAPH IS NULL THEN
      SELECT SEQ_PARAGRAPH.NEXTVAL INTO :NEW.ID_PARAGRAPH FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_PARAGRAPH_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_IMAGE
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_IMAGE_TRG" 
BEFORE INSERT ON T_IMAGE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_IMAGE IS NULL THEN
      SELECT SEQ_IMAGE.NEXTVAL INTO :NEW.ID_IMAGE FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_IMAGE_TRG" ENABLE;

--------------------------------------------------------
--  CREATE TRIGGER FOR PK ID_DATA
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "T_DATA_TRG" 
BEFORE INSERT ON T_DATA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID_DATA IS NULL THEN
      SELECT SEQ_DATA.NEXTVAL INTO :NEW.ID_DATA FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_DATA_TRG" ENABLE;
