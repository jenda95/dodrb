--------------------------------------------------------------------------
--************************ INSERTS TESTING DATA ************************--
--------------------------------------------------------------------------

--------------------------------------------------------
--  INSERT INTO T_TEMPLATE
--------------------------------------------------------
INSERT INTO "T_TEMPLATE" (ID_TEMPLATE, NAME, URL) 
VALUES (
    SEQ_TEMPLATE.nextval, 
    concat('Template ', SEQ_TEMPLATE.currval), 
    concat('/templ-', SEQ_TEMPLATE.currval)
);

--------------------------------------------------------
--  RESET SWEQUENCE - MAIN LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_ML;
CREATE SEQUENCE SEQ_ORDER_ML START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF T_MAIN_LINK FOR TEMPLATE
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_TEMPLATE
--------------------------------------------------------
INSERT INTO "T_TEMPLATE" (ID_TEMPLATE, NAME, URL) 
VALUES (
    SEQ_TEMPLATE.nextval, 
    concat('Template ', SEQ_TEMPLATE.currval), 
    concat('/templ-', SEQ_TEMPLATE.currval)
);

--------------------------------------------------------
--  RESET SWEQUENCE - MAIN LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_ML;
CREATE SEQUENCE SEQ_ORDER_ML START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF T_MAIN_LINK FOR TEMPLATE
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_MAIN_LINK
--------------------------------------------------------
INSERT INTO "T_MAIN_LINK" (ID_MAIN_LINK, NAME, LINK_ORDER, URL, ID_TEMPLATE) 
VALUES (
    SEQ_MAIN_LINK.nextval, 
    concat('Main Page ', SEQ_MAIN_LINK.currval), 
    SEQ_ORDER_ML.nextval, concat('/main-page-', 
    SEQ_MAIN_LINK.currval), 
    SEQ_TEMPLATE.currval
);

--------------------------------------------------------
--  RESET SWEQUENCE - SECONDARY LINK ORDER 
--------------------------------------------------------
DROP SEQUENCE SEQ_ORDER_SL;
CREATE SEQUENCE SEQ_ORDER_SL START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;


--------------------------------------------------------
--  INSERT 1-N ROWS OF SECONDARY LINK FOR MAIN LINK
--------------------------------------------------------
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
--------------------------------------------------------
--  INSERT INTO T_SECONDARY_LINK
--------------------------------------------------------
INSERT INTO "T_SECONDARY_LINK" (ID_SECONDARY_LINK, NAME, URL, LINK_ORDER, ID_MAIN_LINK) 
VALUES (
    SEQ_SECONDARY_LINK.nextval, 
    concat('Secondary Link ', SEQ_SECONDARY_LINK.currval), 
    concat('/sl-', SEQ_SECONDARY_LINK.currval), 
    SEQ_ORDER_SL.nextval, 
    SEQ_MAIN_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_CONTENT
--------------------------------------------------------
INSERT INTO "T_CONTENT" (ID_CONTENT, MAIN_TITLE, ID_SECONDARY_LINK) 
VALUES (
    SEQ_CONTENT.nextval, 
    concat('Content ', SEQ_CONTENT.currval), 
    SEQ_SECONDARY_LINK.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_IMAGE
--------------------------------------------------------
INSERT INTO "T_IMAGE" (ID_IMAGE, NAME, SUFFIX, ID_ARTICLE) 
VALUES (
    SEQ_IMAGE.nextval, 
    concat('img ', SEQ_IMAGE.currval), 
    'jpg', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_DATA
--------------------------------------------------------
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);
INSERT INTO "T_DATA" (ID_DATA, DATA, ID_IMAGE) 
VALUES (
    SEQ_DATA.nextval, 
    'AB13',
    SEQ_IMAGE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);

--------------------------------------------------------
--  INSERT INTO T_ARTICLE
--------------------------------------------------------
INSERT INTO "T_ARTICLE" (ID_ARTICLE, NAME, ID_CONTENT) 
VALUES (
    SEQ_ARTICLE.nextval, 
    concat('Article ', SEQ_ARTICLE.currval), 
    SEQ_CONTENT.currval
);

--------------------------------------------------------
--  INSERT INTO T_PARAGRAPH
--------------------------------------------------------
INSERT INTO "T_PARAGRAPH" (ID_PARAGRAPH, NAME, CONTENT, ID_ARTICLE) 
VALUES (
    SEQ_PARAGRAPH.nextval, 
    concat('Paragraph ', SEQ_PARAGRAPH.currval), 
    'some text', 
    SEQ_ARTICLE.currval
);




