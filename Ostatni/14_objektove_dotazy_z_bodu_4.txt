SET AUTOTRACE ON;

-- 1. seznam v�ech prim�rn� odkaz� v�etn� v�ech jejich parametr� pro vykreslen� na str�nce
SELECT
    ml.NAME AS "N�zev",
    CONCAT(ml.template_ref.url, ml.URL) AS "Absolutn� URL",
    ml.template_ref.name as "N�zev templatu",
    ml.link_order as "Po�ad� v templatu",
    COUNT(DISTINCT sl.id_secondary_link) AS "Po�et pododkaz�",
    COUNT(DISTINCT pa.id_paragraph) AS "Po�et paragraf�",
    COUNT(DISTINCT art.id_article) AS "Po�et �l�nk�",
    COUNT(DISTINCT img.id_image) AS "Po�et obr�zk�",
    COUNT(DISTINCT dt.id_data) AS "Po�et datov�ch soubot�"
FROM Obj_MAIN_LINK ml
JOIN obj_SECONDARY_LINK sl ON sl.main_link_ref = REF(ml) 
JOIN obj_content co ON co.secondary_link_ref = REF(sl)
JOIN obj_article art ON art.content_ref = REF(co)
JOIN obj_paragraph pa ON pa.article_ref = REF(art)
JOIN obj_image img ON img.article_ref = REF(art)
JOIN obj_data dt ON dt.image_ref = REF(img) 
group by ml.NAME, CONCAT(ml.template_ref.url, ml.URL), ml.template_ref.name, ml.link_order
--WHERE t.id_template = 3
ORDER BY ml.template_ref.name;


-- 2. seznam v�ech sekund�rn�ch odkaz� v�etn� v�ech jejich parametr� pro vykreslen� na str�nce, kter� spadaj� pod ur�it� prim�rn� odkaz

SELECT
    sl.name AS "N�zev",
    CONCAT(CONCAT(sl.main_link_ref.template_ref.URL, sl.main_link_ref.URL),sl.url) AS "Absolutn� URL",
    sl.main_link_ref.template_ref.name as "N�zev templatu",
    sl.main_link_ref.name as "N�zev main linku",
    sl.main_link_ref.link_order as "Po�ad� main linku v templatu",
    sl.link_order as "Po�ad� v main linku",
    COUNT(DISTINCT pa.id_paragraph) AS "Po�et paragraf�",
    COUNT(DISTINCT art.id_article) AS "Po�et �l�nk�",
    COUNT(DISTINCT img.id_image) AS "Po�et obr�zk�",
    COUNT(DISTINCT dt.id_data) AS "Po�et datov�ch soubot�"
FROM obj_secondary_link sl
JOIN obj_content co ON co.secondary_link_ref = REF(sl)
JOIN obj_article art ON art.content_ref = REF(co)
JOIN obj_paragraph pa ON pa.article_ref = REF(art)
JOIN obj_image img ON img.article_ref = REF(art)
JOIN obj_data dt ON dt.image_ref = REF(img)

WHERE sl.main_link_ref.id_main_link = 2 or sl.main_link_ref.id_main_link = 13 
group by sl.name, CONCAT(CONCAT(sl.main_link_ref.template_ref.URL, sl.main_link_ref.URL),sl.url), sl.main_link_ref.template_ref.name, sl.main_link_ref.name, sl.main_link_ref.link_order, sl.link_order
ORDER BY sl.main_link_ref.template_ref.name, sl.link_order;


-- 3. seznam v�ech �l�nk�, kter� spadaj� pod ur�it� sekund�rn� odkaz
SELECT art.id_article, art.name, art.content_ref.secondary_link_ref.main_link_ref.template_ref.name, art.content_ref.secondary_link_ref.main_link_ref.name, art.content_ref.secondary_link_ref.name
FROM obj_article art
JOIN obj_paragraph pa ON pa.article_ref = REF(art)
JOIN obj_image img ON img.article_ref = REF(art)
JOIN obj_data dt ON dt.image_ref = REF(img)
ORDER BY art.content_ref.secondary_link_ref.main_link_ref.template_ref.name, art.content_ref.secondary_link_ref.link_order;


-- 4. seznam v�ech obr�zk�, kter� se v��ou k dan�mu �l�nku

SELECT img.id_image, img.name, img.suffix, count(distinct  dt.id_data)
FROM obj_image img
JOIN obj_data dt ON dt.image_ref = REF(img)
WHERE img.article_ref.id_article = 10
group by img.id_image, img.name, img.suffix
order by img.id_image;

-- 5. seznam odstavc�, kter� spadaj� pod konkr�tn� �l�nek

SELECT pa.id_paragraph, pa.name, pa.content
FROM obj_paragraph pa
WHERE pa.article_ref.id_article = 4
order by pa.name;


SET AUTOTRACE OFF;